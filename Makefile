genproto:
	rm -rf gen

	buf generate --path proto/genproto

	find . "./gen/js" -name "*.js" -type f | xargs sed -i -e '/google_api_annotations_pb/d'
	node ./ts_export.js
