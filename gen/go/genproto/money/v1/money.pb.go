// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        (unknown)
// source: genproto/money/v1/money.proto

package moneypb

import (
	v1 "bitbucket.org/entrlcom/open-genproto/gen/go/genproto/iso/4217/v1"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Money struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Amount   string     `protobuf:"bytes,1,opt,name=amount,proto3" json:"amount,omitempty"`                                           // Amount (9.99).
	Currency v1.Iso4217 `protobuf:"varint,2,opt,name=currency,proto3,enum=genproto.iso.iso4217.v1.Iso4217" json:"currency,omitempty"` // Currency.
}

func (x *Money) Reset() {
	*x = Money{}
	if protoimpl.UnsafeEnabled {
		mi := &file_genproto_money_v1_money_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Money) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Money) ProtoMessage() {}

func (x *Money) ProtoReflect() protoreflect.Message {
	mi := &file_genproto_money_v1_money_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Money.ProtoReflect.Descriptor instead.
func (*Money) Descriptor() ([]byte, []int) {
	return file_genproto_money_v1_money_proto_rawDescGZIP(), []int{0}
}

func (x *Money) GetAmount() string {
	if x != nil {
		return x.Amount
	}
	return ""
}

func (x *Money) GetCurrency() v1.Iso4217 {
	if x != nil {
		return x.Currency
	}
	return v1.Iso4217(0)
}

var File_genproto_money_v1_money_proto protoreflect.FileDescriptor

var file_genproto_money_v1_money_proto_rawDesc = []byte{
	0x0a, 0x1d, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6d, 0x6f, 0x6e, 0x65, 0x79,
	0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x6f, 0x6e, 0x65, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x11, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x6d, 0x6f, 0x6e, 0x65, 0x79, 0x2e,
	0x76, 0x31, 0x1a, 0x22, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x69, 0x73, 0x6f,
	0x2f, 0x34, 0x32, 0x31, 0x37, 0x2f, 0x76, 0x31, 0x2f, 0x69, 0x73, 0x6f, 0x34, 0x32, 0x31, 0x37,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x5d, 0x0a, 0x05, 0x4d, 0x6f, 0x6e, 0x65, 0x79, 0x12,
	0x16, 0x0a, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x3c, 0x0a, 0x08, 0x63, 0x75, 0x72, 0x72, 0x65,
	0x6e, 0x63, 0x79, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x20, 0x2e, 0x67, 0x65, 0x6e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x69, 0x73, 0x6f, 0x2e, 0x69, 0x73, 0x6f, 0x34, 0x32, 0x31, 0x37,
	0x2e, 0x76, 0x31, 0x2e, 0x49, 0x73, 0x6f, 0x34, 0x32, 0x31, 0x37, 0x52, 0x08, 0x63, 0x75, 0x72,
	0x72, 0x65, 0x6e, 0x63, 0x79, 0x42, 0x47, 0x5a, 0x45, 0x62, 0x69, 0x74, 0x62, 0x75, 0x63, 0x6b,
	0x65, 0x74, 0x2e, 0x6f, 0x72, 0x67, 0x2f, 0x65, 0x6e, 0x74, 0x72, 0x6c, 0x63, 0x6f, 0x6d, 0x2f,
	0x6f, 0x70, 0x65, 0x6e, 0x2d, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x67, 0x65,
	0x6e, 0x2f, 0x67, 0x6f, 0x2f, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6d, 0x6f,
	0x6e, 0x65, 0x79, 0x2f, 0x76, 0x31, 0x3b, 0x6d, 0x6f, 0x6e, 0x65, 0x79, 0x70, 0x62, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_genproto_money_v1_money_proto_rawDescOnce sync.Once
	file_genproto_money_v1_money_proto_rawDescData = file_genproto_money_v1_money_proto_rawDesc
)

func file_genproto_money_v1_money_proto_rawDescGZIP() []byte {
	file_genproto_money_v1_money_proto_rawDescOnce.Do(func() {
		file_genproto_money_v1_money_proto_rawDescData = protoimpl.X.CompressGZIP(file_genproto_money_v1_money_proto_rawDescData)
	})
	return file_genproto_money_v1_money_proto_rawDescData
}

var file_genproto_money_v1_money_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_genproto_money_v1_money_proto_goTypes = []interface{}{
	(*Money)(nil),   // 0: genproto.money.v1.Money
	(v1.Iso4217)(0), // 1: genproto.iso.iso4217.v1.Iso4217
}
var file_genproto_money_v1_money_proto_depIdxs = []int32{
	1, // 0: genproto.money.v1.Money.currency:type_name -> genproto.iso.iso4217.v1.Iso4217
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_genproto_money_v1_money_proto_init() }
func file_genproto_money_v1_money_proto_init() {
	if File_genproto_money_v1_money_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_genproto_money_v1_money_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Money); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_genproto_money_v1_money_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_genproto_money_v1_money_proto_goTypes,
		DependencyIndexes: file_genproto_money_v1_money_proto_depIdxs,
		MessageInfos:      file_genproto_money_v1_money_proto_msgTypes,
	}.Build()
	File_genproto_money_v1_money_proto = out.File
	file_genproto_money_v1_money_proto_rawDesc = nil
	file_genproto_money_v1_money_proto_goTypes = nil
	file_genproto_money_v1_money_proto_depIdxs = nil
}
