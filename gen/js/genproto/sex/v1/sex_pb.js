// source: genproto/sex/v1/sex.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global =
    (typeof globalThis !== 'undefined' && globalThis) ||
    (typeof window !== 'undefined' && window) ||
    (typeof global !== 'undefined' && global) ||
    (typeof self !== 'undefined' && self) ||
    (function () { return this; }).call(null) ||
    Function('return this')();

goog.exportSymbol('proto.genproto.sex.v1.Sex', null, global);
/**
 * @enum {number}
 */
proto.genproto.sex.v1.Sex = {
  SEX_UNSPECIFIED: 0,
  SEX_FEMALE: 1,
  SEX_MALE: 2
};

goog.object.extend(exports, proto.genproto.sex.v1);
