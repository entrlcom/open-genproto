// package: genproto.sex.v1
// file: genproto/sex/v1/sex.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Sex {
    SEX_UNSPECIFIED = 0,
    SEX_FEMALE = 1,
    SEX_MALE = 2,
}
