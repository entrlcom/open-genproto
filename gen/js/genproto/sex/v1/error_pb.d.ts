// package: genproto.sex.v1
// file: genproto/sex/v1/error.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Error {
    ERROR_UNSPECIFIED = 0,
    ERROR_SEX_INVALID = 1,
}
