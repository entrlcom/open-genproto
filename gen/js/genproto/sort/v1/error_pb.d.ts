// package: genproto.sort.v1
// file: genproto/sort/v1/error.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Error {
    ERROR_UNSPECIFIED = 0,
    ERROR_SORT_FIELD_INVALID = 1,
    ERROR_SORT_ORDER_INVALID = 2,
}
