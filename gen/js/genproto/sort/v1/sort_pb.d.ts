// package: genproto.sort.v1
// file: genproto/sort/v1/sort.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Sort extends jspb.Message { 
    getField(): string;
    setField(value: string): Sort;
    getOrder(): SortOrder;
    setOrder(value: SortOrder): Sort;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Sort.AsObject;
    static toObject(includeInstance: boolean, msg: Sort): Sort.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Sort, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Sort;
    static deserializeBinaryFromReader(message: Sort, reader: jspb.BinaryReader): Sort;
}

export namespace Sort {
    export type AsObject = {
        field: string,
        order: SortOrder,
    }
}

export enum SortOrder {
    SORT_ORDER_UNSPECIFIED = 0,
    SORT_ORDER_DESC = 1,
    SORT_ORDER_ASC = 2,
}
