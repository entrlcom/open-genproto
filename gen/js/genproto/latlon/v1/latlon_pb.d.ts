// package: genproto.latlon.v1
// file: genproto/latlon/v1/latlon.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class LatLon extends jspb.Message { 
    getLat(): number;
    setLat(value: number): LatLon;
    getLon(): number;
    setLon(value: number): LatLon;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): LatLon.AsObject;
    static toObject(includeInstance: boolean, msg: LatLon): LatLon.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: LatLon, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): LatLon;
    static deserializeBinaryFromReader(message: LatLon, reader: jspb.BinaryReader): LatLon;
}

export namespace LatLon {
    export type AsObject = {
        lat: number,
        lon: number,
    }
}
