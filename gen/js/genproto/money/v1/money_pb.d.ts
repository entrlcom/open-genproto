// package: genproto.money.v1
// file: genproto/money/v1/money.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";
import * as genproto_iso_4217_v1_iso4217_pb from "../../../genproto/iso/4217/v1/iso4217_pb";

export class Money extends jspb.Message { 
    getAmount(): string;
    setAmount(value: string): Money;
    getCurrency(): genproto_iso_4217_v1_iso4217_pb.Iso4217;
    setCurrency(value: genproto_iso_4217_v1_iso4217_pb.Iso4217): Money;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Money.AsObject;
    static toObject(includeInstance: boolean, msg: Money): Money.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Money, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Money;
    static deserializeBinaryFromReader(message: Money, reader: jspb.BinaryReader): Money;
}

export namespace Money {
    export type AsObject = {
        amount: string,
        currency: genproto_iso_4217_v1_iso4217_pb.Iso4217,
    }
}
