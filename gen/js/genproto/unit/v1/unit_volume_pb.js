// source: genproto/unit/v1/unit_volume.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global =
    (typeof globalThis !== 'undefined' && globalThis) ||
    (typeof window !== 'undefined' && window) ||
    (typeof global !== 'undefined' && global) ||
    (typeof self !== 'undefined' && self) ||
    (function () { return this; }).call(null) ||
    Function('return this')();

goog.exportSymbol('proto.genproto.unit.v1.Volume', null, global);
/**
 * @enum {number}
 */
proto.genproto.unit.v1.Volume = {
  VOLUME_UNSPECIFIED: 0,
  VOLUME_ML: 1,
  VOLUME_L: 2,
  VOLUME_FT3: 3,
  VOLUME_M3: 4,
  VOLUME_BBL: 5
};

goog.object.extend(exports, proto.genproto.unit.v1);
