// package: genproto.unit.v1
// file: genproto/unit/v1/unit_volume.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Volume {
    VOLUME_UNSPECIFIED = 0,
    VOLUME_ML = 1,
    VOLUME_L = 2,
    VOLUME_FT3 = 3,
    VOLUME_M3 = 4,
    VOLUME_BBL = 5,
}
