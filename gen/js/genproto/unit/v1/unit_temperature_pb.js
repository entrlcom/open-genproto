// source: genproto/unit/v1/unit_temperature.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global =
    (typeof globalThis !== 'undefined' && globalThis) ||
    (typeof window !== 'undefined' && window) ||
    (typeof global !== 'undefined' && global) ||
    (typeof self !== 'undefined' && self) ||
    (function () { return this; }).call(null) ||
    Function('return this')();

goog.exportSymbol('proto.genproto.unit.v1.Temperature', null, global);
/**
 * @enum {number}
 */
proto.genproto.unit.v1.Temperature = {
  TEMPERATURE_UNSPECIFIED: 0,
  TEMPERATURE_C: 1,
  TEMPERATURE_F: 2,
  TEMPERATURE_K: 3,
  TEMPERATURE_R: 4
};

goog.object.extend(exports, proto.genproto.unit.v1);
