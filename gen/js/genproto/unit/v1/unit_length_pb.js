// source: genproto/unit/v1/unit_length.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global =
    (typeof globalThis !== 'undefined' && globalThis) ||
    (typeof window !== 'undefined' && window) ||
    (typeof global !== 'undefined' && global) ||
    (typeof self !== 'undefined' && self) ||
    (function () { return this; }).call(null) ||
    Function('return this')();

goog.exportSymbol('proto.genproto.unit.v1.Length', null, global);
/**
 * @enum {number}
 */
proto.genproto.unit.v1.Length = {
  LENGTH_UNSPECIFIED: 0,
  LENGTH_MM: 1,
  LENGTH_CM: 2,
  LENGTH_M: 3,
  LENGTH_KM: 4,
  LENGTH_IN: 5,
  LENGTH_FT: 6,
  LENGTH_YD: 7,
  LENGTH_MI: 8
};

goog.object.extend(exports, proto.genproto.unit.v1);
