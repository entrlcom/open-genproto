// source: genproto/unit/v1/unit_weight.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global =
    (typeof globalThis !== 'undefined' && globalThis) ||
    (typeof window !== 'undefined' && window) ||
    (typeof global !== 'undefined' && global) ||
    (typeof self !== 'undefined' && self) ||
    (function () { return this; }).call(null) ||
    Function('return this')();

goog.exportSymbol('proto.genproto.unit.v1.Weight', null, global);
/**
 * @enum {number}
 */
proto.genproto.unit.v1.Weight = {
  WEIGHT_UNSPECIFIED: 0,
  WEIGHT_MG: 1,
  WEIGHT_CT: 2,
  WEIGHT_G: 3,
  WEIGHT_OZ: 4,
  WEIGHT_LB: 5,
  WEIGHT_KG: 6,
  WEIGHT_TON: 7
};

goog.object.extend(exports, proto.genproto.unit.v1);
