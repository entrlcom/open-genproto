// package: genproto.unit.v1
// file: genproto/unit/v1/unit_temperature.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Temperature {
    TEMPERATURE_UNSPECIFIED = 0,
    TEMPERATURE_C = 1,
    TEMPERATURE_F = 2,
    TEMPERATURE_K = 3,
    TEMPERATURE_R = 4,
}
