// package: genproto.unit.v1
// file: genproto/unit/v1/unit_weight.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Weight {
    WEIGHT_UNSPECIFIED = 0,
    WEIGHT_MG = 1,
    WEIGHT_CT = 2,
    WEIGHT_G = 3,
    WEIGHT_OZ = 4,
    WEIGHT_LB = 5,
    WEIGHT_KG = 6,
    WEIGHT_TON = 7,
}
