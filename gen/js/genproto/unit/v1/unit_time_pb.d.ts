// package: genproto.unit.v1
// file: genproto/unit/v1/unit_time.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Time {
    TIME_UNSPECIFIED = 0,
    TIME_SECOND = 1,
    TIME_MINUTE = 2,
    TIME_HOUR = 3,
    TIME_DAY = 4,
    TIME_WEEK = 5,
    TIME_MONTH = 6,
    TIME_YEAR = 7,
}
