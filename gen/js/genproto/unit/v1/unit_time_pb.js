// source: genproto/unit/v1/unit_time.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global =
    (typeof globalThis !== 'undefined' && globalThis) ||
    (typeof window !== 'undefined' && window) ||
    (typeof global !== 'undefined' && global) ||
    (typeof self !== 'undefined' && self) ||
    (function () { return this; }).call(null) ||
    Function('return this')();

goog.exportSymbol('proto.genproto.unit.v1.Time', null, global);
/**
 * @enum {number}
 */
proto.genproto.unit.v1.Time = {
  TIME_UNSPECIFIED: 0,
  TIME_SECOND: 1,
  TIME_MINUTE: 2,
  TIME_HOUR: 3,
  TIME_DAY: 4,
  TIME_WEEK: 5,
  TIME_MONTH: 6,
  TIME_YEAR: 7
};

goog.object.extend(exports, proto.genproto.unit.v1);
