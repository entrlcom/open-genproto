// package: genproto.unit.v1
// file: genproto/unit/v1/unit_length.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Length {
    LENGTH_UNSPECIFIED = 0,
    LENGTH_MM = 1,
    LENGTH_CM = 2,
    LENGTH_M = 3,
    LENGTH_KM = 4,
    LENGTH_IN = 5,
    LENGTH_FT = 6,
    LENGTH_YD = 7,
    LENGTH_MI = 8,
}
