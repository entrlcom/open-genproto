// source: genproto/zodiacsign/v1/zodiacsign.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global =
    (typeof globalThis !== 'undefined' && globalThis) ||
    (typeof window !== 'undefined' && window) ||
    (typeof global !== 'undefined' && global) ||
    (typeof self !== 'undefined' && self) ||
    (function () { return this; }).call(null) ||
    Function('return this')();

goog.exportSymbol('proto.genproto.zodiacsign.v1.ZodiacSign', null, global);
/**
 * @enum {number}
 */
proto.genproto.zodiacsign.v1.ZodiacSign = {
  ZODIAC_SIGN_UNSPECIFIED: 0,
  ZODIAC_SIGN_ARIES: 1,
  ZODIAC_SIGN_TAURUS: 2,
  ZODIAC_SIGN_GEMINI: 3,
  ZODIAC_SIGN_CANCER: 4,
  ZODIAC_SIGN_LEO: 5,
  ZODIAC_SIGN_VIRGO: 6,
  ZODIAC_SIGN_LIBRA: 7,
  ZODIAC_SIGN_SCORPIO: 8,
  ZODIAC_SIGN_SAGITTARIUS: 9,
  ZODIAC_SIGN_CAPRICORN: 10,
  ZODIAC_SIGN_AQUARIUS: 11,
  ZODIAC_SIGN_PISCES: 12
};

goog.object.extend(exports, proto.genproto.zodiacsign.v1);
