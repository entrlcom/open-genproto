// package: genproto.pagination.v1
// file: genproto/pagination/v1/pagination.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Pagination extends jspb.Message { 

    hasCursor(): boolean;
    clearCursor(): void;
    getCursor(): CursorPagination | undefined;
    setCursor(value?: CursorPagination): Pagination;

    hasPage(): boolean;
    clearPage(): void;
    getPage(): PagePagination | undefined;
    setPage(value?: PagePagination): Pagination;

    getPaginationCase(): Pagination.PaginationCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Pagination.AsObject;
    static toObject(includeInstance: boolean, msg: Pagination): Pagination.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Pagination, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Pagination;
    static deserializeBinaryFromReader(message: Pagination, reader: jspb.BinaryReader): Pagination;
}

export namespace Pagination {
    export type AsObject = {
        cursor?: CursorPagination.AsObject,
        page?: PagePagination.AsObject,
    }

    export enum PaginationCase {
        PAGINATION_NOT_SET = 0,
        CURSOR = 1,
        PAGE = 2,
    }

}

export class CursorPagination extends jspb.Message { 
    getPageToken(): string;
    setPageToken(value: string): CursorPagination;
    getPageSize(): number;
    setPageSize(value: number): CursorPagination;
    getDirection(): PaginationDirection;
    setDirection(value: PaginationDirection): CursorPagination;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CursorPagination.AsObject;
    static toObject(includeInstance: boolean, msg: CursorPagination): CursorPagination.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CursorPagination, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CursorPagination;
    static deserializeBinaryFromReader(message: CursorPagination, reader: jspb.BinaryReader): CursorPagination;
}

export namespace CursorPagination {
    export type AsObject = {
        pageToken: string,
        pageSize: number,
        direction: PaginationDirection,
    }
}

export class CursorPaginationResponse extends jspb.Message { 
    getTotal(): number;
    setTotal(value: number): CursorPaginationResponse;
    getPageToken(): string;
    setPageToken(value: string): CursorPaginationResponse;
    getNext(): boolean;
    setNext(value: boolean): CursorPaginationResponse;
    getPrev(): boolean;
    setPrev(value: boolean): CursorPaginationResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CursorPaginationResponse.AsObject;
    static toObject(includeInstance: boolean, msg: CursorPaginationResponse): CursorPaginationResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CursorPaginationResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CursorPaginationResponse;
    static deserializeBinaryFromReader(message: CursorPaginationResponse, reader: jspb.BinaryReader): CursorPaginationResponse;
}

export namespace CursorPaginationResponse {
    export type AsObject = {
        total: number,
        pageToken: string,
        next: boolean,
        prev: boolean,
    }
}

export class PagePagination extends jspb.Message { 
    getPage(): number;
    setPage(value: number): PagePagination;
    getPageSize(): number;
    setPageSize(value: number): PagePagination;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PagePagination.AsObject;
    static toObject(includeInstance: boolean, msg: PagePagination): PagePagination.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PagePagination, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PagePagination;
    static deserializeBinaryFromReader(message: PagePagination, reader: jspb.BinaryReader): PagePagination;
}

export namespace PagePagination {
    export type AsObject = {
        page: number,
        pageSize: number,
    }
}

export class PagePaginationResponse extends jspb.Message { 
    getTotal(): number;
    setTotal(value: number): PagePaginationResponse;
    getNext(): boolean;
    setNext(value: boolean): PagePaginationResponse;
    getPrev(): boolean;
    setPrev(value: boolean): PagePaginationResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PagePaginationResponse.AsObject;
    static toObject(includeInstance: boolean, msg: PagePaginationResponse): PagePaginationResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PagePaginationResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PagePaginationResponse;
    static deserializeBinaryFromReader(message: PagePaginationResponse, reader: jspb.BinaryReader): PagePaginationResponse;
}

export namespace PagePaginationResponse {
    export type AsObject = {
        total: number,
        next: boolean,
        prev: boolean,
    }
}

export enum PaginationDirection {
    PAGINATION_DIRECTION_UNSPECIFIED = 0,
    PAGINATION_DIRECTION_NEXT = 1,
    PAGINATION_DIRECTION_PREV = 2,
}
