// source: genproto/pagination/v1/error.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global =
    (typeof globalThis !== 'undefined' && globalThis) ||
    (typeof window !== 'undefined' && window) ||
    (typeof global !== 'undefined' && global) ||
    (typeof self !== 'undefined' && self) ||
    (function () { return this; }).call(null) ||
    Function('return this')();

goog.exportSymbol('proto.genproto.pagination.v1.Error', null, global);
/**
 * @enum {number}
 */
proto.genproto.pagination.v1.Error = {
  ERROR_UNSPECIFIED: 0,
  ERROR_PAGINATION_INVALID: 1,
  ERROR_PAGINATION_DIRECTION_INVALID: 2,
  ERROR_PAGINATION_PAGE_INVALID: 3,
  ERROR_PAGINATION_PAGE_SIZE_INVALID: 4,
  ERROR_PAGINATION_PAGE_TOKEN_INVALID: 5
};

goog.object.extend(exports, proto.genproto.pagination.v1);
