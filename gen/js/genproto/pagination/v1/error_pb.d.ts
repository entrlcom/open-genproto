// package: genproto.pagination.v1
// file: genproto/pagination/v1/error.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export enum Error {
    ERROR_UNSPECIFIED = 0,
    ERROR_PAGINATION_INVALID = 1,
    ERROR_PAGINATION_DIRECTION_INVALID = 2,
    ERROR_PAGINATION_PAGE_INVALID = 3,
    ERROR_PAGINATION_PAGE_SIZE_INVALID = 4,
    ERROR_PAGINATION_PAGE_TOKEN_INVALID = 5,
}
