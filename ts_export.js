const fs = require('fs');
const glob = require('glob');

const tsExt = '.d.ts';

glob(`gen/js/**/*${tsExt}`, (err, files) => {
	if (err) throw new Error(err);

	for (let i = 0; i < files.length; i += 1) {
		const path = files[i];
		const names = path.split('/');
		const pathToFile = names.slice(0, -1).join('/');
		const indexPath = `${pathToFile}/index.ts`;

		if (!fs.existsSync(indexPath)) {
			fs.writeFileSync(indexPath, '');
		}

		fs.appendFileSync(indexPath, `export * from './${names[names.length - 1].replace(tsExt, '')}';\n`);

		const dirs =
			fs.readdirSync(`${pathToFile}`, { withFileTypes: true })
			.filter(dirent => dirent.isDirectory())
			.map(dirent => dirent.name)

		if (dirs.length > 0) {
			for (let d = 0; d < dirs.length; d += 1) {
				const dir = dirs[d];
				const indexFile = fs.readFileSync(indexPath);
				if (!(indexFile.indexOf(`'./${dir}';`) >= 0)) {
					fs.appendFileSync(indexPath, `export * from './${dir}';\n`);
				}
			}
		}
	}
});
