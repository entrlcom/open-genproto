module bitbucket.org/entrlcom/open-genproto

go 1.19

require (
	bitbucket.org/entrlcom/protoc-gen-validator v0.0.0-20230531161855-b416c034fb10
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230530153820-e85fd2cbaebc
	google.golang.org/protobuf v1.30.0
)

require golang.org/x/exp v0.0.0-20230522175609-2e198f4a06a1 // indirect
